﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace asp.net_web_api_assignment.Model
{
    [Table("Movie")]
    public class Movie
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }
        [Required]
        [MaxLength(50)]
        public string Genre { get; set; }
        [Required]
        [MaxLength(20)]
        public string Release_Year { get; set; }
        [Required]
        [MaxLength(50)]
        public string Director { get; set; }
        [MaxLength(300)]
        public string Img_Url { get; set; }
        [MaxLength(300)]
        public string Trailer { get; set; }

        // Foreign Key to Franchise ID - One to Many Relationship
        public int FranchiseID { get; set; }
        public Franchise Franchise { get; set; }

        // Collection of Characters to represent the Many to Many relationship
        public ICollection<Character> Characters { get; set; }
    }
}
