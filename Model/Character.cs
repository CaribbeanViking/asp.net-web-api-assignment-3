﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace asp.net_web_api_assignment.Model
{
    [Table("Character")]
    public class Character
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)] // Auto increment
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }
        [Required]
        [MaxLength(100)]
        public string Alias { get; set; }
        [Required]
        [MaxLength(50)]
        public string Gender { get; set; }
        [Required]
        [MaxLength(300)]
        public string Picture { get; set; }
        public ICollection<Movie> Movies { get; set; } // Collection of Movies defining a TO MANY relation from Character to Movies
    }
}
