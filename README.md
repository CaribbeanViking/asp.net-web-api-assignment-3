## Release
Version: 1.0
Date: 2022-08-25

## Description
This is a movie catalogue web API created using ASP.NET. It allows users to add movies and associated characters/franchises. It has several end points whit CRUD-operations as well as some other GET/UPDATE functionality.

## Installation

A deployed version doesn't need installation to use. To run locally, SSMS is needed along with SQL Server and Tools in Visual Studio.

## Using the application

Run Program.cs and the local SSMS application. Swagger should be launched automatically which allows a user to use all endpoints.

## Help

Thouroghly commented for assistance using summaries. Documentation (summaries) is also visible with the endpoint in Swagger through Swashbuckle.

## Contributors

https://gitlab.com/CaribbeanViking
https://gitlab.com/saintgod
https://gitlab.com/oskar.nyman

## Contributing

No one else than above mentioned.

## Known Bugs

When deleting movie/character/franchise and post new, auto increment does not autimatically replace missing Id. Post always gives a new Id.
