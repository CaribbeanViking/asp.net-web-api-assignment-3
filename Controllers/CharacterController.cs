﻿using asp.net_web_api_assignment.Model;
using asp.net_web_api_assignment_3.Data;
using asp.net_web_api_assignment_3.DTO.CharacterDTOs;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net.Mime;

namespace asp.net_web_api_assignment_3.Controllers
{
    [Route("api/v1/characters")]
    [ApiController]

    // Documentation
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    // Defines Character controller behaviour 
    public class CharacterController : ControllerBase
    {

        // Dependency injections
        private readonly MovieCatalogueDBContext _context;
        private readonly IMapper _mapper;

        // Constructor
        public CharacterController(MovieCatalogueDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Fetch all characters.
        /// </summary>
        /// <returns>An IMapper object of type ReadcharacterDTO as a list</returns>
        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<ReadCharacterDTO>>> GetAllCharacters()
        {
            return _mapper.Map<List<ReadCharacterDTO>>(await _context.Characters
                .Include(c => c.Movies) // Include movies associated with Character
                .ToListAsync());
        }


        /// <summary>
        /// Fetch character by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A character as an Imapper object.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadCharacterDTO>> GetCharacterById(int id)
        {
            Character character = await _context.Characters.Include(f => f.Movies)
                .Where(c => c.Id == id).FirstAsync();

            if (character == null) // If current Id has no Character...
            {
                return NotFound();
            }

            return _mapper.Map<ReadCharacterDTO>(character);
        }

        /// <summary>
        /// Add a new character to the API.
        /// </summary>
        /// <param name="character"></param>
        /// <returns>Ok</returns>
        [HttpPost]
        public async Task<ActionResult> AddCharacter([FromBody] CreateCharacterDTO character)
        {
            var domainCharacter = _mapper.Map<Character>(character); // Map from DTO to domain object/model
            _context.Characters.Add(domainCharacter); // Add to Character context
            await _context.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Replace/Update a character, find by id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="character"></param>
        [HttpPut("{id}")]
        public async Task<ActionResult> ReplaceCharacterByID(int id, [FromBody] UpdateCharacterDTO character)
        {
            var domainCharacter = _mapper.Map<Character>(character); // Map from DTO to domain object/model

            _context.Entry(domainCharacter).State = EntityState.Modified; // Update
            await _context.SaveChangesAsync();
            return NoContent();
        }

        /// <summary>
        /// Delete character by Id.
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteCharacterByID(int id)
        {
            var character = _context.Characters.Find(id);
            if (character == null)
            {
                return NotFound();
            }
            _context.Characters.Remove(character); // Delete Character from DB
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
