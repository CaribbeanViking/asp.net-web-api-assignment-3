﻿using asp.net_web_api_assignment.Model;
using asp.net_web_api_assignment_3.Data;
using asp.net_web_api_assignment_3.DTO.CharacterDTOs;
using asp.net_web_api_assignment_3.DTO.MovieDTOs;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net.Mime;

namespace asp.net_web_api_assignment_3.Controllers
{
    [Route("api/v1/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MovieController : ControllerBase
    {
        private readonly MovieCatalogueDBContext _context;
        private readonly IMapper _mapper;
        public MovieController(MovieCatalogueDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Fetch all movies from API
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<ReadMovieDTO>>> GetAllMovies()
        {
            return _mapper.Map<List<ReadMovieDTO>>(await _context.Movies
                .Include(c => c.Characters)
                .ToListAsync());
        }

        /// <summary>
        /// Fetch a specific movie by its Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A mapper object of type ReadMovieDTO</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadMovieDTO>> GetById(int id)
        {
            // Includes a list of Characters where the input id matches the character id.

            Movie movie = await _context.Movies.Include(m => m.Characters)
            .Where(f => f.Id == id).FirstAsync();
            return _mapper.Map<ReadMovieDTO>(movie);
        }

        /// <summary>
        /// Get all characters that star in a movie.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A mapper object of type ReadMovieDTO as a list</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<List<ReadCharacterDTO>>> GetCharactersInMovieById(int id)
        {
            // A Character will be displayed including the Movies that have the same id as the input id.

            return _mapper.Map<List<ReadCharacterDTO>>(await _context.Characters.Include(m => m.Movies)
                .Where(c => c.Movies.Select(i => i.Id).Contains(id)).ToListAsync());
        }

        /// <summary>
        /// Add a new movie entry to the API.
        /// </summary>
        /// <param name="movieDTO"></param>
        /// <returns>A CreatedAtAction object that returns a new function call.</returns>
        [HttpPost]
        public async Task<ActionResult> AddMovie([FromBody] CreateMovieDTO movieDTO)
        {
            // Maps a input CreateMovieDTO to a Movie
            var domainMovie = _mapper.Map<Movie>(movieDTO);
            _context.Movies.Add(domainMovie);
            await _context.SaveChangesAsync();
            // Maps the Movie to a ReadMovieDTO
            var readMovieDTO = _mapper.Map<ReadMovieDTO>(domainMovie);

            // Returns a satus 201 if the "GetById" function works given the movie id and a ReadMovieDTO object.
            return CreatedAtAction("GetById", new { Id = domainMovie.Id }, readMovieDTO);
        }

        /// <summary>
        /// Fetch a Movie by id, then overwrite associated characters with new characters from user input.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characters"></param>
        [HttpPut("{id}/characters")]
        public async Task<ActionResult> UpdateMovieCharacters(int id, List<int> characters)
        {
            // Creates a Movie object that will house the specified Movie by Id.
            Movie movieToUpdateCharacters = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.Id == id)
                .FirstAsync();

            List<Character> character = new();

            // Goes through each element in the List of characters (ints)
            // Finds the Character by Id in the context DbSet of Characters
            // If not null then we add the character to a new list
            foreach (int characterId in characters)
            {
                Character ch = await _context.Characters.FindAsync(characterId);
                if (ch == null)
                    return BadRequest("Characters does not exist!");
                character.Add(ch);
            }

            // Updates the Character list with the new characters
            movieToUpdateCharacters.Characters = character;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

        /// <summary>
        /// Update a specific movie, find by Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updateMovie"></param>

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateMovie(int id, [FromBody] UpdateMovieDTO updateMovie)
        {
            if (id != updateMovie.Id)
            {
                return BadRequest();
            }

            var domainMovie = _mapper.Map<Movie>(updateMovie);
            _context.Entry(domainMovie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

        /// <summary>
        /// Delete a movie by id.
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteMovieById(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
