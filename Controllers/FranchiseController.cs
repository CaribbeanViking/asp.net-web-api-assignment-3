﻿using asp.net_web_api_assignment.Model;
using asp.net_web_api_assignment_3.Data;
using asp.net_web_api_assignment_3.DTO.CharacterDTOs;
using asp.net_web_api_assignment_3.DTO.FranchinseDTOs;
using asp.net_web_api_assignment_3.DTO.MovieDTOs;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net.Mime;

namespace asp.net_web_api_assignment_3.Controllers
{
    [Route("api/v1/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchiseController : ControllerBase
    {
        private readonly MovieCatalogueDBContext _context;
        private readonly IMapper _mapper;

        public FranchiseController(MovieCatalogueDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Fetches all currently created franchises from the API.
        /// </summary>
        /// <returns>An IMapper object of the type FranchiseReadDTO</returns>
        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetAllFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _context.Franchises
                .Include(m => m.Movies)
                .ToListAsync());
        }

        /// <summary>
        /// Fetch one franchise by a specific Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An IMapper object of the type FranchiseReadDTO</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchiseById(int id)
        {
            Franchise franchise = await _context.Franchises.Include(f => f.Movies)
                .Where(f => f.Id == id).FirstAsync();
            return _mapper.Map<FranchiseReadDTO>(franchise);

            //var franchiseReadDTOList = _mapper.Map<List<FranchiseReadDTO>>(await _context.Franchises
            //    .Include(m => m.Movies)
            //    .ToListAsync());

            //var franchiseReadDTO = franchiseReadDTOList.Find(f => f.Id == id);

            //if (franchiseReadDTO == null)
            //{
            //    return NotFound();
            //}

            //return franchiseReadDTO;
        }


        /// <summary>
        /// Add a new franchise to the API.
        /// </summary>
        /// <param name="franchiseDTO"></param>
        /// <returns>A createdAtAction object that returns a new function call</returns>
        [HttpPost]
        public async Task<ActionResult> AddFranchise([FromBody] FranchiseCreateDTO franchiseDTO)
        {
            var domainFranchise = _mapper.Map<Franchise>(franchiseDTO);
            _context.Franchises.Add(domainFranchise);
            await _context.SaveChangesAsync();
            var readFranchiseDTO = _mapper.Map<FranchiseReadDTO>(domainFranchise);

            return CreatedAtAction("GetFranchiseById", new { id = domainFranchise.Id }, readFranchiseDTO);
        }

        /// <summary>
        /// Get all movies that belongs to a specific franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An IMapper object of the type FranchiseReadDTO as a list</returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<List<ReadMovieDTO>>> GetMoviesInFranchiseById(int id)
        {
            return _mapper.Map<List<ReadMovieDTO>>(await _context.Movies.Include(c => c.Characters)
                .Where(m => m.FranchiseID == id)
                .ToListAsync());
        }

        /// <summary>
        /// Fetches all characters from the movies that belong to a specific franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An IMapper object of the type FranchiseReadDTO as a list</returns>
        [HttpGet("{id}/movies/characters")]
        public async Task<ActionResult<List<ReadCharacterDTO>>> GetCharactersInFranchiseById(int id)
        {
            return _mapper.Map<List<ReadCharacterDTO>>(await _context.Characters.Include(m => m.Movies)
                .Where(c => c.Movies.Select(m => m.FranchiseID).Contains(id)).ToListAsync());
        }

        /// <summary>
        /// Fetch a franchise, then overwrites associated movies with new movies from user input.
        /// </summary>
        /// <param name="id"></param>
        /// <param> name="movies"></param>
        [HttpPut("{id}/movies")]
        public async Task<ActionResult> UpdateFranchiseMovies(int id, List<int> movies)
        {
            Franchise franchiseToUpdateMovies = await _context.Franchises
                .Include(f => f.Movies)
                .Where(f => f.Id == id)
                .FirstAsync();

            List<Movie> movie = new();
            foreach (int movieId in movies)
            {
                Movie move = await _context.Movies.FindAsync(movieId);
                if (move == null)
                    return BadRequest("Movies does not exist!");
                movie.Add(move);
            }

            franchiseToUpdateMovies.Movies = movie;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

        /// <summary>
        /// Function that deletes a franchise, find it by specific Id.
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteFranchiseByID(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
