﻿using asp.net_web_api_assignment.Model;
using asp.net_web_api_assignment_3.DTO.FranchinseDTOs;
using AutoMapper;

namespace asp.net_web_api_assignment_3.DTO.FranchiseProfiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                    .MapFrom(c => c.Movies
                    .Select(c => c.Id)
                    .ToArray())).ReverseMap();

            CreateMap<Franchise, FranchiseCreateDTO>().ReverseMap();
            CreateMap<Franchise, FranchiseUpdateDTO>().ReverseMap();
        }
    }
}
