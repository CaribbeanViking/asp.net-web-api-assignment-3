﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace asp.net_web_api_assignment_3.DTO.CharacterDTOs
{
    // Data Transfer Object to define what will be shown in front when reading a Character
    public class ReadCharacterDTO
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }
        [Required]
        [MaxLength(100)]
        public string Alias { get; set; }
        [Required]
        [MaxLength(50)]
        public string Gender { get; set; }
        [Required]
        [MaxLength(300)]
        public string Picture { get; set; }
        public List<int> Movies { get; set; }
    }
}
