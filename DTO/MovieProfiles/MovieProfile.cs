﻿using asp.net_web_api_assignment.Model;
using asp.net_web_api_assignment_3.DTO.MovieDTOs;
using AutoMapper;

namespace asp.net_web_api_assignment_3.DTO.MovieProfiles

{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            // Maps a relationship between Movie and ReadMovieDTO
            CreateMap<Movie, ReadMovieDTO>()
                // For a ReadMovieDTO FranchiseId member will be mapped from the Movie FranchiseID.
                .ForMember(mdto => mdto.FranchiseId, opt => opt
                    .MapFrom(m => m.FranchiseID))
                // For a ReadMovieDTO Characters integer member List will be mapped from the Movie Character member object list in which the Id will be used.
                .ForMember(mdto => mdto.Characters, opt => opt
                    .MapFrom(m => m.Characters.Select(m => m.Id).ToArray()))
                // Reverse enables this to be a two-way mapping.
                .ReverseMap();

            CreateMap<Movie, CreateMovieDTO>()
                .ForMember(mdto => mdto.FranchiseId, opt => opt
                    .MapFrom(m => m.FranchiseID)).ReverseMap();

            CreateMap<Movie, UpdateMovieDTO>()
               .ForMember(mdto => mdto.FranchiseId, opt => opt
                   .MapFrom(m => m.FranchiseID)).ReverseMap();
        }

    }
}