﻿using System.ComponentModel.DataAnnotations;

namespace asp.net_web_api_assignment_3.DTO.FranchinseDTOs
{
    public class FranchiseUpdateDTO
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        [Required]
        [MaxLength(300)]
        public string Description { get; set; }
    }
}
