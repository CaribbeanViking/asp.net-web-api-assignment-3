﻿using System.ComponentModel.DataAnnotations;

namespace asp.net_web_api_assignment_3.DTO.MovieDTOs
{
    /// <summary>
    /// <param>This Data Transfer Object represents the data that is needed in order to CREATE a new Movie.</param> 
    /// <param>It does not include the many to many relationship with Characters.</param>
    /// There needs to be an existing Franchise as an input in order for this to work.
    /// [MaxLength..] is used to restrict the allocated memory for each string.
    /// </summary>
    public class CreateMovieDTO
    {
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }
        [Required]
        [MaxLength(50)]
        public string Genre { get; set; }
        [Required]
        [MaxLength(20)]
        public string Release_Year { get; set; }
        [Required]
        [MaxLength(50)]
        public string Director { get; set; }
        [MaxLength(300)]
        public string Img_Url { get; set; }
        [MaxLength(300)]
        public string Trailer { get; set; }
        public int FranchiseId { get; set; }
    }
}
