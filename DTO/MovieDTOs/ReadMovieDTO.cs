﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace asp.net_web_api_assignment_3.DTO.MovieDTOs
{
    /// <summary>
    /// <param>This Data Transfer Object represents the data that is needed in order to READ a Movie.</param> 
    /// <p>It includes the many to many relationship with Characters as we want to display this when displaying information about the Movie.</p>
    /// [MaxLength..] is used to restrict the allocated memory for each string.
    /// </summary>
    public class ReadMovieDTO
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }
        [Required]
        [MaxLength(50)]
        public string Genre { get; set; }
        [Required]
        [MaxLength(20)]
        public string Release_Year { get; set; }
        [Required]
        [MaxLength(50)]
        public string Director { get; set; }
        [MaxLength(300)]
        public string Img_Url { get; set; }
        [MaxLength(300)]
        public string Trailer { get; set; }
        public int FranchiseId { get; set; }
        public List<int> Characters { get; set; }
    }
}
