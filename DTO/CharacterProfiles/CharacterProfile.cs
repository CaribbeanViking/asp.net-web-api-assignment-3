﻿using asp.net_web_api_assignment.Model;
using asp.net_web_api_assignment_3.DTO.CharacterDTOs;
using AutoMapper;

namespace asp.net_web_api_assignment_3.DTO.CharacterProfiles
{
    public class CharacterProfile : Profile
    {
        // Defines mapping between DTO and Model
        public CharacterProfile()
        {
            CreateMap<Character, ReadCharacterDTO>() // Mapping between Character and ReadCharacterDTO setting array of Integers with the Id's of the Movies acc. to MANY-TO-MANY relationship.
                .ForMember(cdto => cdto.Movies, opt => opt
                    .MapFrom(c => c.Movies
                    .Select(c => c.Id)
                    .ToArray()));

            CreateMap<Character, CreateCharacterDTO>().ReverseMap(); // Standard mapping between Character and CreateCharacterDTO
            CreateMap<Character, UpdateCharacterDTO>().ReverseMap(); // Standard mapping between Character and UpdateCharacterDTO
        }
    }
}
