﻿using asp.net_web_api_assignment.Model;

namespace asp.net_web_api_assignment_3.Data
{
    public class DummyData
    {
        public static IEnumerable<Movie> GetMovieDummyData()
        {
            IEnumerable<Movie> Dummy_Movies = new List<Movie>()
            {
                new Movie
                {
                    Id = 1,
                    Title = "Lord of the Rings: Fellowship of the ring",
                    Genre = "Fantasy",
                    Release_Year = "2001",
                    Director = "Peter Jackson",
                    Img_Url = "hejhej.com",
                    Trailer = "Trailertrail",
                    FranchiseID = 1
                },
                new Movie
                {
                    Id = 2,
                    Title = "Lord of the Rings: Two Towers",
                    Genre = "Fantasy",
                    Release_Year = "2002",
                    Director = "Peter Jackson",
                    Img_Url = "hejhej.com",
                    Trailer = "Trailertrail",
                    FranchiseID = 1
                },
                 new Movie
                {
                    Id = 3,
                    Title = "Lord of the Rings: Return of the king",
                    Genre = "Fantasy",
                    Release_Year = "2003",
                    Director = "Peter Jackson",
                    Img_Url = "hejhej.com",
                    Trailer = "Trailertrail",
                    FranchiseID = 1
                },
                  new Movie
                {
                    Id = 4,
                    Title = "Tin tin",
                    Genre = "Fantasy",
                    Release_Year = "2009",
                    Director = "Peter Jackson",
                    Img_Url = "hejhej.com",
                    Trailer = "Trailertrail",
                    FranchiseID = 2
                }
            };
            return Dummy_Movies;
        }
        public static IEnumerable<Franchise> GetFranchises()
        {
            IEnumerable<Franchise> franchises = new List<Franchise>()
            {
                new Franchise
                {
                    Id = 1,
                    Name = "Lord of the rings",
                    Description = "Depiction of JRR tolkiens books",
                },
                new Franchise
                {
                    Id = 2,
                    Name = "Adventures of Tintin",
                    Description = "Follow the epic adventures of Tintin!",
                }
            };
            return franchises;
        }
        public static IEnumerable<Character> GetCharacters()
        {
            IEnumerable<Character> characters = new List<Character>()
            {
                new Character
                {
                    Id = 1,
                    Name = "Smeagol",
                    Alias = "Gollum",
                    Gender = "Undefined",
                    Picture = "Perfect"
                },
                new Character
                {
                    Id = 2,
                    Name = "Captain Haddok",
                    Alias = "The drunk",
                    Gender = "Male",
                    Picture = "image"
,
                },
                 new Character
                {
                    Id = 3,
                    Name = "Milou",
                    Alias = "The dog",
                    Gender = "Male dog",
                    Picture = "cutedogimage.png"
,
                },
                  new Character
                {
                    Id = 4,
                    Name = "Legolas",
                    Alias = "The Elf",
                    Gender = "Elf male",
                    Picture = "legolas.png"
,
                }
            };
            return characters;
        }
    }
}
