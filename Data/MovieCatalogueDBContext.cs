﻿using asp.net_web_api_assignment.Model;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace asp.net_web_api_assignment_3.Data
{
    public class MovieCatalogueDBContext : DbContext
    {
        public MovieCatalogueDBContext(DbContextOptions options) : base(options) { } // Pass changed options back to parent class

        // Define DbSets. Representation of all enitites in this context that can be queried from DB.
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Character> Characters { get; set; }

        // Define structure of initial data when model is created
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Insert Dummy Data
            modelBuilder.Entity<Movie>().HasData(DummyData.GetMovieDummyData());
            modelBuilder.Entity<Character>().HasData(DummyData.GetCharacters());
            modelBuilder.Entity<Franchise>().HasData(DummyData.GetFranchises());

            // Define specific relationship between Movies and Characters. MANY-TO-MANY relationship.
            modelBuilder.Entity<Character>()
                .HasMany(m => m.Movies) // MANY-TO-MANY
                .WithMany(c => c.Characters) // MANY-TO-MANY
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie", // Join table
                    r => r.HasOne<Movie>().WithMany().HasForeignKey("MoviesId"), // Right table
                    l => l.HasOne<Character>().WithMany().HasForeignKey("CharactersId"), // Left table
                    je =>
                    {
                        je.HasKey("MoviesId", "CharactersId");
                        je.HasData( // Specify what Character belongs to what movie
                            new { CharactersId = 1, MoviesId = 1 },
                            new { CharactersId = 2, MoviesId = 1 },
                            new { CharactersId = 4, MoviesId = 1 },
                            new { CharactersId = 1, MoviesId = 2 },
                            new { CharactersId = 2, MoviesId = 2 },
                            new { CharactersId = 4, MoviesId = 2 },
                            new { CharactersId = 1, MoviesId = 3 },
                            new { CharactersId = 4, MoviesId = 3 },
                            new { CharactersId = 2, MoviesId = 4 },
                            new { CharactersId = 3, MoviesId = 4 },
                            new { CharactersId = 1, MoviesId = 4 }
                        );
                    });
        }


    }
}
